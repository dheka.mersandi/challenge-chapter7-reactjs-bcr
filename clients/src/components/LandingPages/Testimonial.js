// import React from 'react';
// import OwlCarousel from 'react-owl-carousel';
// import 'owl.carousel/dist/assets/owl.carousel.css';
// import 'owl.carousel/dist/assets/owl.theme.default.css';

const Testimonial = () => {
  return (
    <div className="testi" id="testi">
      <h3 className="text-center testi-text">Testimonial</h3>
      <p className="mt-2 text-center">Berbagai review positif dari para pelanggan kami</p>
      <div className="carousel slide" id="carouselExampleControls" data-bs-ride="carousel">
        <div className="carousel-inner">
          
        <div class="carousel-item active">
                <div class="card mb-3" style={{maxWidth: '540px'}}>
                    <div class="row g-0">
                        <div class="col-lg-3">
                            <img src="./assets/person-1.svg" class="img-fluid rounded-start person" alt="..."></img>
                        </div>
                        <div class="col-lg-9">
                            <div class="card-body">
                            <img src="images/rate.png" alt="" class="rate"/>
                            <p class="card-text">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                            <h5 class="card-title"><strong>John Dee 32, Bromo</strong></h5>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="card mb-3" style={{maxWidth: '540px'}}>
                    <div class="row g-0">
                        <div class="col-lg-3">
                            <img src="./images/person-2.svg" class="img-fluid rounded-start person" alt="..."></img>
                        </div>
                        <div class="col-lg-9">
                            <div class="card-body">
                            <img src="images/rate.png" alt="" class="rate"/>
                            <p class="card-text">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                            <h5 class="card-title"><strong>John Dee 32, Bromo</strong></h5>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="card mb-3" style={{maxWidth: '540px'}}>
                    <div class="row g-0">
                        <div class="col-lg-3">
                            <img src="./images/person-1.svg" class="img-fluid rounded-start person" alt="..."></img>
                        </div>
                        <div class="col-lg-9">
                            <div class="card-body">
                                <img src="images/rate.png" alt="" class="rate"/>
                            <p class="card-text">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                            <h5 class="card-title"><strong>John Dee 32, Bromo</strong></h5>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="card mb-3" style={{maxWidth: '540px'}}>
                    <div class="row g-0">
                        <div class="col-lg-3">
                            <img src="./images/person-2.svg" class="img-fluid rounded-start person" alt="..."></img>
                        </div>
                        <div class="col-lg-9">
                            <div class="card-body">
                                <img src="images/rate.png" alt="" class="rate"/>
                            <p class="card-text">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                            <h5 class="card-title"><strong>John Dee 32, Bromo</strong></h5>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="card mb-3" style={{maxWidth: '540px'}}>
                    <div class="row g-0">
                        <div class="col-lg-3">
                            <img src="./images/person-2.svg" class="img-fluid rounded-start person" alt="..."></img>
                        </div>
                        <div class="col-lg-9">
                            <div class="card-body">
                                <img src="images/rate.png" alt="" class="rate"/>
                            <p class="card-text">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                            <h5 class="card-title"><strong>John Dee 32, Bromo</strong></h5>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className="container-btn d-flex justify-content-center">
          <button id="prev" className="btn border rounded-circle" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
            <img src="img/Vector.svg" alt="" />
          </button>
          <button id="next" className="btn border rounded-circle" data-bs-target="#carouselExampleControls" data-bs-slide="next">
            <img src="img/Vector2.svg" alt="" />
          </button>
        </div>
      </div>
    </div>
  );
};

export default Testimonial;

// const Testimonial = () => {
//   return (
//     <section id="testimonial" class="container">
//         <div class="row">
//             <div class="col-12">
//                 <h2 class="testimonial-title"><strong>Testimonial</strong></h2>
//                 <p class="testimonial-title">Berbagai review positif dari para pelanggan kami</p>
//             </div>
//         </div>
//         <div class="owl-carousel owl-theme">
// <OwlCarousel className='owl-theme' loop margin={10} nav>
// <div class="item row-4">
//                 <div class="card mb-3" style={{maxWidth: '540px'}}>
//                     <div class="row g-0">
//                         <div class="col-lg-3">
//                             <img src="./images/person-1.svg" class="img-fluid rounded-start person" alt="..."></img>
//                         </div>
//                         <div class="col-lg-9">
//                             <div class="card-body">
//                             <img src="images/rate.png" alt="" class="rate"/>
//                             <p class="card-text">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
//                             <h5 class="card-title"><strong>John Dee 32, Bromo</strong></h5>
//                         </div>
//                         </div>
//                     </div>
//                 </div>
//             </div>
//             <div class="item row-4">
//                 <div class="card mb-3" style={{maxWidth: '540px'}}>
//                     <div class="row g-0">
//                         <div class="col-lg-3">
//                             <img src="./images/person-2.svg" class="img-fluid rounded-start person" alt="..."></img>
//                         </div>
//                         <div class="col-lg-9">
//                             <div class="card-body">
//                             <img src="images/rate.png" alt="" class="rate"/>
//                             <p class="card-text">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
//                             <h5 class="card-title"><strong>John Dee 32, Bromo</strong></h5>
//                         </div>
//                         </div>
//                     </div>
//                 </div>
//             </div>
//             <div class="item row-4">
//                 <div class="card mb-3" style={{maxWidth: '540px'}}>
//                     <div class="row g-0">
//                         <div class="col-lg-3">
//                             <img src="./images/person-1.svg" class="img-fluid rounded-start person" alt="..."></img>
//                         </div>
//                         <div class="col-lg-9">
//                             <div class="card-body">
//                                 <img src="images/rate.png" alt="" class="rate"/>
//                             <p class="card-text">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
//                             <h5 class="card-title"><strong>John Dee 32, Bromo</strong></h5>
//                         </div>
//                         </div>
//                     </div>
//                 </div>
//             </div>
//             <div class="item row-4">
//                 <div class="card mb-3" style={{maxWidth: '540px'}}>
//                     <div class="row g-0">
//                         <div class="col-lg-3">
//                             <img src="./images/person-2.svg" class="img-fluid rounded-start person" alt="..."></img>
//                         </div>
//                         <div class="col-lg-9">
//                             <div class="card-body">
//                                 <img src="images/rate.png" alt="" class="rate"/>
//                             <p class="card-text">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
//                             <h5 class="card-title"><strong>John Dee 32, Bromo</strong></h5>
//                         </div>
//                         </div>
//                     </div>
//                 </div>
//             </div>
//             <div class="item row-4">
//                 <div class="card mb-3" style={{maxWidth: '540px'}}>
//                     <div class="row g-0">
//                         <div class="col-lg-3">
//                             <img src="./images/person-2.svg" class="img-fluid rounded-start person" alt="..."></img>
//                         </div>
//                         <div class="col-lg-9">
//                             <div class="card-body">
//                                 <img src="images/rate.png" alt="" class="rate"/>
//                             <p class="card-text">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
//                             <h5 class="card-title"><strong>John Dee 32, Bromo</strong></h5>
//                         </div>
//                         </div>
//                     </div>
//                 </div>
//             </div>
// </OwlCarousel>
            
//         </div>
//     </section> 
//   );
// };

// export default Testimonial;