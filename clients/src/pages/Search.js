import { useEffect } from "react";
import Filter from "../components/LandingPages/Filter";
import Footer from "../components/LandingPages/Footer";
import JumbotronNoButton from "../components/LandingPages/JumbotronNoButton";
import Navbar from "../components/LandingPages/Navbar";

const Search = () => {
  useEffect(() => {
    document.title = "Search Cars";
  }, []);
  return (
    <>
      <Navbar />
      <JumbotronNoButton />
      <Filter />
      <div className="container">
        <Footer />
      </div>
    </>
  );
};

export default Search;
