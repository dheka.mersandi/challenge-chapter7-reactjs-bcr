import Jumbotron from "../components/LandingPages/Jumbotron";
import Navbar from "../components/LandingPages/Navbar";
import OurServices from "../components/LandingPages/OurServices";
import WhyUs from "../components/LandingPages/WhyUs";
// import Testimonial from "../components/Testimonial";
import Sewa from "../components/LandingPages/Sewa";
import Faq from "../components/LandingPages/Faq";
import Footer from "../components/LandingPages/Footer";
import { useEffect } from "react";
import Carousel from "../components/Caraousel/Carousel";

const Home = () => {
  useEffect(() => {
    document.title = "Homepage";
  }, []);

  return (
    <>
      <Navbar />
      <Jumbotron />
      <div className="container">
        <OurServices />
        <WhyUs />
      </div>
      <Carousel />
      <div className="container">
        <Sewa />
        <Faq />
        <Footer />
      </div>
    </>
  );
};

export default Home;
